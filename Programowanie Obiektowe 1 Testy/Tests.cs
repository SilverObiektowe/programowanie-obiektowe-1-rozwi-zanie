﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using Programowanie_Obiektowe_1_Silver.NET;

namespace Programowanie_Obiektowe_1_Testy
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Klasa_Car_Zostala_Napisana()
        {
            Assembly assembly = typeof(Program).Assembly;

            Type[] types = assembly.GetTypes();
            Type carType = null;

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    carType = type;
                }
            }

            Assert.IsNotNull(carType);
        }

        [TestMethod]
        public void Klasa_Car_Posiada_Pole_Color()
        {
            bool passed = false;            
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    FieldInfo[] fields = type.GetFields();

                    foreach (FieldInfo fieldInfo in fields)
                    {
                        if (fieldInfo.Name.ToLower() == "color")
                        {
                            passed = true;
                            break;
                        }
                    }
                }
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Klasa_Car_Posiada_Pole_typNadwozia()
        {
            bool passed = false;
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    FieldInfo[] fields = type.GetFields();

                    foreach (FieldInfo fieldInfo in fields)
                    {
                        if (fieldInfo.Name.ToLower() == "typnadwozia")
                        {
                            passed = true;
                            break;
                        }
                    }
                }
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Klasa_Car_Posiada_Metode_Jedz()
        {
            bool passed = false;
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    MethodInfo[] methods = type.GetMethods();

                    foreach (MethodInfo methodsInfo in methods)
                    {
                        if (methodsInfo.Name.ToLower() == "jedz")
                        {
                            passed = true;
                            break;
                        }
                    }
                }
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Klasa_Car_Posiada_Metode_Tankuj()
        {
            bool passed = false;
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    MethodInfo[] methods = type.GetMethods();

                    foreach (MethodInfo methodsInfo in methods)
                    {
                        if (methodsInfo.Name.ToLower() == "tankuj")
                        {
                            passed = true;
                            break;
                        }
                    }
                }
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Klasa_Car_Posiada_Metode_Hamuj()
        {
            bool passed = false;
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    MethodInfo[] methods = type.GetMethods();

                    foreach (MethodInfo methodsInfo in methods)
                    {
                        if (methodsInfo.Name.ToLower() == "hamuj")
                        {
                            passed = true;
                            break;
                        }
                    }
                }
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Metoda_Jedz_Jest_Zgodna_Z_Trescia()
        {
            bool passed = false;
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    MethodInfo[] methods = type.GetMethods();

                    foreach (MethodInfo methodInfo in methods)
                    {
                        if (methodInfo.Name.ToLower() == "jedz")
                        {
                            ParameterInfo[] parameters = methodInfo.GetParameters();

                            if (parameters.Length == 0 && 
                                methodInfo.ReturnType == typeof(void))
                            {
                                passed = true;
                            }
                            break;
                        }
                    }
                }
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Metoda_Hamuj_Jest_Zgodna_Z_Trescia()
        {
            bool passed = false;
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    MethodInfo[] methods = type.GetMethods();

                    foreach (MethodInfo methodInfo in methods)
                    {
                        if (methodInfo.Name.ToLower() == "hamuj")
                        {
                            ParameterInfo[] parameters = methodInfo.GetParameters();

                            if (parameters.Length == 0 &&
                                methodInfo.ReturnType == typeof(bool))
                            {
                                passed = true;
                            }
                            break;
                        }
                    }
                }
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Metoda_Tankuj_Jest_Zgodna_Z_Trescia()
        {
            bool passed = true;
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();
            Type carType = null;

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    carType = type;
                    MethodInfo[] methods = type.GetMethods();

                    foreach (MethodInfo methodInfo in methods)
                    {
                        if (methodInfo.Name.ToLower() == "tankuj")
                        {
                            ParameterInfo[] parameters = methodInfo.GetParameters();

                            if (!(parameters.Length == 1 &&
                                methodInfo.ReturnType == typeof(void)))
                            {
                                passed = false;
                            }

                            foreach (ParameterInfo parameter in parameters)
                            {
                                if (parameter.ParameterType != typeof(Int32))
                                {
                                    passed = false;
                                }
                            }

                            break;
                        }
                    }
                }
            }

            if (carType == null)
            {
                passed = false;
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Klasa_Car_Posiada_Konstruktor()
        {
            bool passed = true;
            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();
            Type carType = null;

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    carType = type;
                    ConstructorInfo[] constructors = type.GetConstructors();

                    bool containsTwoParameterCtor = false;
                    bool haveConstructorWithOnlyStringsParameter = false;
                    foreach (ConstructorInfo constructor in constructors)
                    {
                        ParameterInfo[] parameters = constructor.GetParameters();
                        bool hasOnlyStringsParameter = true;

                        if (parameters.Length == 2)
                        {
                            containsTwoParameterCtor = true;
                            foreach (ParameterInfo parameter in parameters)
                            {
                                if (parameter.ParameterType != typeof(string))
                                {
                                    hasOnlyStringsParameter = false;
                                }
                            }
                        }

                        if (hasOnlyStringsParameter)
                        {
                            haveConstructorWithOnlyStringsParameter = true;
                        }


                    }

                    if (!containsTwoParameterCtor)
                    {
                        passed = false;
                    }

                    if (!haveConstructorWithOnlyStringsParameter)
                    {
                        passed = false;
                    }
                }
            }

            if (carType == null)
            {
                passed = false;
            }

            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void Zostaly_Stworzone_3_Obiekty_Klasy_Car()
        {
            Program.Main(null);
            bool passed = true;

            Assembly assembly = typeof(Program).Assembly;
            Type[] types = assembly.GetTypes();
            Type carType = null;

            foreach (Type type in types)
            {
                if (type.Name == "Car")
                {
                    carType = type;
                }
            }

            if (ForTests.Car1.GetType() != carType)
                passed = false;

            if (ForTests.Car2.GetType() != carType)
                passed = false;

            if (ForTests.Car3.GetType() != carType)
                passed = false;

            Assert.IsTrue(passed);
        }
    }
}
