﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programowanie_Obiektowe_1_Silver.NET
{
    public partial class FormWyscig : Form
    {
        public FormWyscig()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            Car car1 = new Car("jasnoniebieski", "");
            Car car2 = new Car("ciemnoniebieski", "");
            Car car3 = new Car("zielony", "");

            car1.speed = 2;
            car2.speed = 1;
            car3.speed = 3;

            for (int i = 0; i < 100; i++)
            {
                pictureBoxCar1.Location = new Point(pictureBoxCar1.Location.X + car1.speed ,pictureBoxCar1.Location.Y);
                pictureBoxCar2.Location = new Point(pictureBoxCar2.Location.X + car2.speed, pictureBoxCar2.Location.Y);
                pictureBoxCar3.Location = new Point(pictureBoxCar3.Location.X + car3.speed, pictureBoxCar3.Location.Y);

                System.Threading.Thread.Sleep(50);
            }
        }
    }
}
