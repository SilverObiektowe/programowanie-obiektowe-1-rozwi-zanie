﻿namespace Programowanie_Obiektowe_1_Silver.NET
{
    partial class FormWyscig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStart = new System.Windows.Forms.Button();
            this.pictureBoxCar1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCar2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCar3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCar3)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(22, 193);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(135, 44);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // pictureBoxCar1
            // 
            this.pictureBoxCar1.Image = global::Programowanie_Obiektowe_1_Silver.NET.Properties.Resources.car1;
            this.pictureBoxCar1.Location = new System.Drawing.Point(22, 25);
            this.pictureBoxCar1.Name = "pictureBoxCar1";
            this.pictureBoxCar1.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxCar1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCar1.TabIndex = 1;
            this.pictureBoxCar1.TabStop = false;
            // 
            // pictureBoxCar2
            // 
            this.pictureBoxCar2.Image = global::Programowanie_Obiektowe_1_Silver.NET.Properties.Resources.car2;
            this.pictureBoxCar2.Location = new System.Drawing.Point(22, 81);
            this.pictureBoxCar2.Name = "pictureBoxCar2";
            this.pictureBoxCar2.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxCar2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCar2.TabIndex = 2;
            this.pictureBoxCar2.TabStop = false;
            // 
            // pictureBoxCar3
            // 
            this.pictureBoxCar3.Image = global::Programowanie_Obiektowe_1_Silver.NET.Properties.Resources.car3;
            this.pictureBoxCar3.Location = new System.Drawing.Point(22, 137);
            this.pictureBoxCar3.Name = "pictureBoxCar3";
            this.pictureBoxCar3.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxCar3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCar3.TabIndex = 3;
            this.pictureBoxCar3.TabStop = false;
            // 
            // FormWyscig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 247);
            this.Controls.Add(this.pictureBoxCar3);
            this.Controls.Add(this.pictureBoxCar2);
            this.Controls.Add(this.pictureBoxCar1);
            this.Controls.Add(this.buttonStart);
            this.Name = "FormWyscig";
            this.Text = "Wyscig";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCar3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.PictureBox pictureBoxCar1;
        private System.Windows.Forms.PictureBox pictureBoxCar2;
        private System.Windows.Forms.PictureBox pictureBoxCar3;
    }
}