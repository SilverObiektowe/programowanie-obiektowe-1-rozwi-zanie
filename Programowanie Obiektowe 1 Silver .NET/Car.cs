﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programowanie_Obiektowe_1_Silver.NET
{
    public class Car
    {
        public int speed;

        public string color;
        public string typNadwozia;

        public Car(string color, string typNadwozia)
        {
            this.color = color;
            this.typNadwozia = typNadwozia;
        }

        public void Jedz()
        {

        }

        public bool Hamuj()
        {
            return true;
        }

        public void Tankuj(int ilośćPaliwa)
        {

        }
    }
}
